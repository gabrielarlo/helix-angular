var _main = ((w, $, axios) =>
{
    'use strict';
    const host = 'localhost:8181';

    $(document).ready(() =>
    {
        _global.getConfig('ws_host', host);
        var path = w.location.pathname;

        if (path === '/') {
            $('#content').load('./content/home.html', () => { console.log('Home is Loaded'); });
        } else if (path === '/database' || path === '/database.html') {
            $('#content').load('./content/database.html', () => { console.log('Database is Loaded'); });
        }

        $('#header').load('./layout/header.html', () => { console.log('Header is Loaded'); });
        $('#footer').load('./layout/footer.html', () => { console.log('Footer is Loaded'); });
    });

    return {
        _http: {
            post: ($url, $data = {}) =>
            {
                const host = _global.getItem('config').ws_host;
                return axios.post('//' + host + $url, $data)
                .then(function (response) {
                    return response;
                }).catch(function (error) {
                    return error.response;
                });
            }
        }
    };

})(window, jQuery, axios);

var _global = {
    Http: _main._http,

    toastr: ($html, $type) =>
    {
        let icon = '';
        switch ($type) {
            case 'success':
                icon = `<i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'info':
                icon = `<i class="fa fa-info-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'warning':
                icon = `<i class="fa fa-exclamation-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'danger':
                icon = `<i class="fa fa-times-circle" aria-hidden="true"></i> &nbsp;`;
                break;
        
            default:
                break;
        }
        M.toast({ html: icon + $html, classes: 'bg-' + $type });
    },

    validateEmail: ($email) =>
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String($email).toLowerCase());
    },

    setConfig: ($key, $value) =>
    {
        const _config = _global._retrieve('config') ? _global._retrieve('config') : {};
        _config[$key] = $value;
        _global._store('config', _config);
        return $value;
    },

    getConfig: ($key, $value_if_null = null) =>
    {
        const _config = _global._retrieve('config') ? _global._retrieve('config') : {};
        if (_config[$key]) {
            return _config[$key];
        } else {
            if ($value_if_null) {
                _config[$key] = $value_if_null;
                _global._store('config', _config);
                return $value_if_null;
            } else {
                return _config[$key];
            }
        }
    },

    setItem: ($key, $value) =>
    {
        _global._store($key, $value);
        return $value;
    },

    getItem: ($key, $value_if_null = null) =>
    {
        if (_global._retrieve($key)) {
            return _global._retrieve($key);
        } else {
            if ($value_if_null) {
                _global._store($key, $value_if_null);
                return $value_if_null;
            } else {
                return _global._retrieve($key);
            }
        }
    },
    
    removeItem: ($key) =>
    {
        localStorage.removeItem($key);
        sessionStorage.removeItem($key);
    },

    strip: ($input) =>
    {
        return $('<p>').html($input).text();
    },

    gravatarURL: ($email) =>
    {
        return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5($email);
    },

    _store: ($key, $value) =>
    {
        const _value = typeof $value == 'object' ? JSON.stringify($value) : $value;
        sessionStorage.setItem($key, _value);
        localStorage.setItem($key, _value);
    },

    _retrieve: ($key) =>
    {
        const stored = sessionStorage.getItem($key) ? sessionStorage.getItem($key) : localStorage.getItem($key);
        return JSON.parse(stored);
    },

    onResponse: ($res) =>
    {
        console.log($res);
        if ($res) {
            if ($res.status === 200 || $res.status === 201) {
                _global.toastr($res.data, 'success');
            } else {
                if ($res.data) {
                    _global.toastr($res.data, 'danger');
                } else {
                    _global.toastr('Host Error', 'danger');
                }
            }
        } else {
            _global.toastr('Host Error', 'danger');
        }
    },
};
