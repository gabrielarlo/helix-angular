new Vue({
    el: '#app',

    data: {
        ws: null, // Our websocket
        newMsg: '', // Holds new messages to be sent to the server
        chatContent: '', // A running list of chat messages displayed on the screen
        email: null, // Email address used for grabbing an avatar
        username: null, // Our username
        joined: false // True if email and username have been filled in
    },

    created: function() {
        var self = this;
        this.ws = new WebSocket('ws://' + window.location.host + '/ws');
        this.ws.addEventListener('message', function(e) {
            var msg = JSON.parse(e.data);
            self.chatContent += '<div class="chip">'
                    + '<img src="' + self.gravatarURL(msg.email) + '">' // Avatar
                    + msg.username
                + '</div>'
                + emojione.toImage(msg.message) + '<br/>'; // Parse emojis

            var element = document.getElementById('chat-messages');
            element.scrollTop = element.scrollHeight; // Auto scroll to the bottom
        });
    },

    methods: {
        send: function () {
            if (this.newMsg != '') {
                this.ws.send(
                    JSON.stringify({
                        email: this.email,
                        username: this.username,
                        message: $('<p>').html(this.newMsg).text() // Strip out html
                    }
                ));
                this.newMsg = ''; // Reset newMsg
            }
        },

        join: function () {
            if (!this.email) {
                Materialize.toast('You must enter an email', 2000);
                return
            }
            if (!this.username) {
                Materialize.toast('You must choose a username', 2000);
                return
            }
            this.email = $('<p>').html(this.email).text();
            this.username = $('<p>').html(this.username).text();
            this.joined = true;
        },

        gravatarURL: function(email) {
            return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(email);
        }
    }
});

(function (d, axios) {
    "use strict";

    var inputFile = d.querySelector("#inputFile");
    var divNotification = d.querySelector("#alert");
    var filehash = d.querySelector("#hash");
    var filename = d.querySelector("#filename");
    var dbname = d.querySelector("#dbname");
    dbname.addEventListener("click", getDbNames);
    dbname.addEventListener("change", getCOLNames);

    inputFile.addEventListener("change", addFile);

    var buildFile = d.querySelector("#buildFile");

    var dataMapFile = d.querySelector("#dataMap");
    buildFile.addEventListener("change", getFile);
    dataMapFile.addEventListener("change", dataMap);

    function getCOLNames() {
        var db = document.getElementById("dbname").value;
        var formData = new FormData();
        console.log(db);
        formData.append("db", db);
        post("/col", formData)
            .then(ResponseCOL)
            .catch(ResponseCOL);
    }
    function getDbNames() {
        post("/database")
            .then(ResponseDB)
            .catch(ResponseDB);
    }

    function getFile(e) {
        var file = e.target.files[0]
        if(!file){
            return
        }
        download(file);
    }

    function download(file) {
        var formData = new FormData()
        formData.append("file", file)
        post("/download", formData)
            .then(ResponseHash)
            .catch(ResponseHash);
    }

    function addFile(e) {
        var file = e.target.files[0]
        if(!file){
            return
        }
        upload(file);
    }

    function upload(file) {
        var formData = new FormData()
        formData.append("file", file)
        post("/upload", formData)
            .then(onResponse)
            .catch(onResponse);
    }
    function dataMap(e) {
        var file = e.target.files[0]
        if(!file){
            return
        }
        Mapup(file);
    }

    function Mapup(file) {
        var hash = document.getElementById("hash").value;
        var filename = document.getElementById("filename").value;
        var formData = new FormData()
        formData.append("file", file)
        formData.append("hash", hash)
        formData.append("filename", filename)
        post("/datamap", formData);
    }

    function ResponseDB(response) {
        var t = response.data;
        console.log(response.data);
        t.forEach(function(element) {
            var node = document.createElement("option");
            var textnode = document.createTextNode(element);
            node.appendChild(textnode);
            document.getElementById("dbname").appendChild(node);
            console.log(element);
        });
    }
    function ResponseCOL(response) {
        var t = response.data;
        console.log(response.data);
        t.forEach(function(element) {
            var node = document.createElement("option");
            var textnode = document.createTextNode(element);
            node.appendChild(textnode);
            document.getElementById("colname").appendChild(node);
            console.log(element);
        });
    }

    function ResponseHash(response) {
        filehash.value = response.data[0];
        filename.value = response.data[1];
    }
    function onResponse(response) {
        var className = (response.status !== 400) ? "success" : "error";
        divNotification.value = response.data;
        divNotification.classList.add(className);
        setTimeout(function() {
            divNotification.classList.remove(className);
        }, 3000);
    }

})(document, axios)
