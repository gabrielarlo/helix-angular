((w, $) => {
    'use strict';

    $(document).ready(() => {
        $('#terminal').load('./../../content/component/terminal.html', () => { console.log('Terminal is Loaded') });
        $('#chat').load('./../../content/component/chat.html', () => { console.log('Chat is Loaded') });
        $('#file_flow').load('./../../content/component/file-flow.html', () => { console.log('File Flow is Loaded') });
    });
})(window, jQuery);