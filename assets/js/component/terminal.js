((w, $) => {
    'use strict';

    const socket = io('https://kryptonia.io:2096', {
        path: '/kryptonia',
        forceNew: true,
    });

    $(document).ready(() => {
        println((new Date()).toString());
        println('Welcome to Helix log terminal');
        println('Initiating terminal...');
        println('Started');

        socket.on('user-connected', (data) => {
            println(`UserID: ${data.user_id} is connected`);
            println(`Data: ${JSON.stringify(data)}`);
        });
        socket.on('user-disconnected', (data) => {
            println(`UserID: ${data.user_id} is disconnected`);
            println(`Data: ${JSON.stringify(data)}`);
        });
    });

    function println($log) {
        const log = $('[name=log]');
        log.append(`${$log}\n`);
    }
})(window, jQuery);