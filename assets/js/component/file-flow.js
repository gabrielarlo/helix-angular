// const Global = _global;

var fileFlow = new Vue({
    el: '#fileFlowApp',
    data: {
        upData: {
            text: 'Ready...',
            type: 'text-muted',
        },
        downData: {
            text: 'Ready...',
            type: 'text-muted',
        },
        dataMap: {
            text: 'Ready...',
            type: 'text-muted',
        },
    },

    created: function ()
    {

    },

    methods: {
        upload: function ()
        {
            const upFile = document.getElementById('upfile').files[0];
            if (!upFile) {
                this.status('upData', 'please select file', 'danger');
                return false;
            }
            this.status('upData', 'processing...');
            const formData = new FormData();
            formData.append('file', upFile);
            Global.Http.post('/upload', formData).then(Global.onResponse).catch(Global.onResponse);
            this.status('upData', 'Ready', 'muted');
        },

        download: function ()
        {
            const downFile = document.getElementById('downfile').files[0];
            if (!downFile) {
                this.status('downData', 'please select file', 'danger');
                return false;
            }
            const dataMap = document.getElementById('datamap').files[0];
            if (!dataMap) {
                this.status('dataMap', 'please select file', 'danger');
                return false;
            }
            this.status('downData', 'processing...');
            const formData = new FormData();
            formData.append('file', downFile);
            Global.Http.post('/download', formData).then((data) =>
            {
                console.log(data);
                this.status('dataMap', 'processing...');
                var hash = data.data[0];
                var filename = data.data[1];
                formData.append("file", dataMap);
                formData.append("hash", hash);
                formData.append("filename", filename);
                Global.Http.post('/datamap', formData).then(Global.onResponse).catch(Global.onResponse);
                this.status('downData', 'Ready', 'muted');
                this.status('dataMap', 'Ready', 'muted');
            }
            ).catch(Global.onResponse);

        },

        status: function ($data, $msg, $type = 'info')
        {
            this[$data].text = $msg;
            this[$data].type = 'text-' + $type;
            Global.toastr($msg, $type);
        },
    }
});
