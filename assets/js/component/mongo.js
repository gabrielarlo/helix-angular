const m = mongodb;

var appMongo = new Vue({
    el: '#appMongo',
    data: {
        connection_status: 'Not Connected'
    },

    created: function ()
    {
        console.log('Vue Loaded');
        const MongoClient = m.MongoClient;

        const url = 'mongodb://localhost:27017';

        const client = new MongoClient(url);

        client.connect(function(err) {
            console.log("Connected successfully to server");
                
            client.close();
        });
    },

    methods: {
        
    }
});