const Global = _global;

var chat = new Vue({
    el: '#chatApp',
    data: {
        joined: false,
        ws: null,
        config: {
            ws_host: 'localhost:8181',
        },
        chat_content: '',
        chat_message: '',
        user: {
            email: '',
            username: '',
        },
    },

    created: function ()
    {
        const self = this;
        this.ws = new WebSocket(`ws://${this.config.ws_host}/ws`);
        this.ws.addEventListener('close', () =>
        {
            Global.removeItem('joined');
            Global.removeItem('user');
            this.user = Global.getItem('user', {
                email: '',
                username: '',
            });
            this.joined = Global.getItem('joined', false);
            Global.toastr('Cant connect to host', 'danger');
        });
        
        this.user = Global.getItem('user', this.user);
        this.joined = Global.getItem('joined', this.joined);
        if (this.joined) {
            this.ws.addEventListener('message', function ($e)
            {
                const data = JSON.parse($e.data);
                const gravatar = Global.gravatarURL(data.email);
                self.chat_content += `<div class="chip">
                    <img src="${gravatar}"> ${data.username}
                </div> ${emojione.toImage(data.message)} <br>`;
                var chat_container = document.getElementById('chat-messages');
                chat_container.scrollTop = chat_container.scrollHeight;
            });
        }
    },

    methods: {
        join: function ()
        {
            if (this.user.email.trim().length === 0) {
                Global.toastr('Email is required', 'warning');
                return false;
            }
            if (!Global.validateEmail(this.user.email)) {
                Global.toastr('Invalid Email', 'warning');
                return false;
            }
            if (this.user.username.trim().length === 0) {
                Global.toastr('Username is required', 'warning');
                return false;
            }

            Global.setConfig('ws_host', this.config.ws_host);
            Global.setItem('user', this.user);
            Global.setItem('joined', true);

            this.ws = new WebSocket(`ws://${this.config.ws_host}/ws`);
            this.ws.addEventListener('message', function ($e)
            {
                const msg = JSON.parse($e.data);
                self.chat_content += ``;
            });
            window.location.reload();
        },

        send: function ()
        {
            this.user = Global.getItem('user');

            if (this.chat_message.trim().length > 0) {
                this.ws.send(
                    JSON.stringify({
                        email: Global.strip(this.user.email),
                        username: Global.strip(this.user.username),
                        message: Global.strip(this.chat_message.trim()),
                    })
                );
                this.chat_message = '';
            }
        },
    },
});

console.log(chat.data);